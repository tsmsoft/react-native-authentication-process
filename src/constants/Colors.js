export default {
  BLACK: '#000000',
  WHITE: '#ffffff',
  DARK_GRAY: '#333333',
  GRAY: '#666666',
  LIGHT_GRAY: '#999999',
  METALIC_GRAY: '#E9EAEA',
  DARK_YELLOW: '#FFD500',
  DARK_RED: "#FB3839"
}
