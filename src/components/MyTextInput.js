import React, { useState } from "react";
import { View, TextInput, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import Colors from "../constants/Colors";
import PropTypes from "prop-types";


const MyTextInput = (props) => {

  const { placeholder, placeholderTextColor, value, setText, keyboardType, secureTextEntry, icon } = props;

  return (
    <View style={styles.container}>

      <Icon name={icon} size={22} color={Colors.LIGHT_GRAY} solid style={styles.icon} />

      <TextInput
        style={styles.textInput}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        value={value}
        onChangeText={text => setText(text)}
        keyboardType={keyboardType}
        secureTextEntry={secureTextEntry}
        autoCapitalize="none"
      />

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 50,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: Colors.LIGHT_GRAY,
    marginVertical: 15
  },
  icon: {
    width: '10%',
    marginRight: 10
  },
  textInput: {
    width: '90%',
    height: 50,
    fontSize: 22
  },
});


MyTextInput.propTypes = {
  placeholder: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  value: PropTypes.string.isRequired,
  setText: PropTypes.func.isRequired,
  keyboardType: PropTypes.oneOf(["default", "number-pad", "decimal-pad", "numeric", "email-address", "phone-pad"]),
  secureTextEntry: PropTypes.bool,
  icon: PropTypes.string
};


MyTextInput.defaultProps = {
  placeholder: "",
  keyboardType: "default",
  secureTextEntry: false,
  icon: "user"
};


export default MyTextInput;
