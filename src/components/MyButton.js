import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import Colors from "../constants/Colors";
import PropTypes from "prop-types";

const MyButton = (props) => {

  const { text, containerStyle, textStyle, onPress } = props;

  return (
    <TouchableOpacity
      onPress={onPress}
    >
      <View style={{ ...styles.container, ...containerStyle }}>
        <Text style={{ ...styles.text, ...textStyle }}>
          {text}
        </Text>
      </View>
    </TouchableOpacity>
  );

};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Colors.DARK_RED,
    borderRadius: 3,
    marginVertical: 15,
  },
  text: {
    fontSize: 24,
    textAlign: "center",
    fontWeight: "bold",
    color: Colors.WHITE,
  },
});


MyButton.protoTypes = {
  text: PropTypes.string.isRequired,
  containerStyle: PropTypes.object,
  textStyle: PropTypes.object,
  onPress: PropTypes.func.isRequired,
};

MyButton.defaultProps = {
  containerStyle: {},
  textStyle: {},
};

export default MyButton;
