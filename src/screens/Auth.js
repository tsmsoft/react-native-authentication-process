import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import MyTextInput from "../components/MyTextInput";
import MyButton from "../components/MyButton";
import Colors from "../constants/Colors";

const Auth = () => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [mobilePhone, setMobilePhone] = useState("");
  const [name, setName] = useState("");
  const [isLogin, setIsLogin] = useState(false);

  return (
    <View style={styles.container}>
      <MyTextInput
        placeholder="E-mail"
        placeholderTextColor={Colors.LIGHT_GRAY}
        value={email}
        setText={setEmail}
        keyboardType="email-address"
        icon="envelope"
      />

      <MyTextInput
        placeholder="Password"
        placeholderTextColor={Colors.LIGHT_GRAY}
        value={password}
        setText={setPassword}
        keyboardType="default"
        secureTextEntry={true}
        icon="key"
      />


      {
        isLogin ?

          <>
            <MyButton
              text="Login"
              onPress={() => {
              }}
            />


            <MyButton
              text="Signup"
              onPress={() => {
                setIsLogin(false);
              }}
              containerStyle={{
                backgroundColor: "",
              }}
              textStyle={{
                color: Colors.DARK_GRAY,
                textDecorationLine: "underline",
                fontSize: 20,
              }}
            />

          </>

          :

          <>

            <MyTextInput
              placeholder="Name"
              placeholderTextColor={Colors.LIGHT_GRAY}
              value={name}
              setText={setName}
              keyboardType="default"
            />

            <MyTextInput
              placeholder="Mobile Phone"
              placeholderTextColor={Colors.LIGHT_GRAY}
              value={mobilePhone}
              setText={setMobilePhone}
              keyboardType="default"
              icon="phone"
            />

            <MyButton
              text="Signup"
              onPress={() => {
              }}
            />


            <MyButton
              text="Signin"
              onPress={() => {
                setIsLogin(true);
              }}
              containerStyle={{
                backgroundColor: "",
              }}
              textStyle={{
                color: Colors.DARK_GRAY,
                textDecorationLine: "underline",
                fontSize: 20,
              }}
            />

          </>


      }


    </View>
  );
};


const styles = StyleSheet.create(
{
  container: {
    paddingHorizontal: 30,
  },
},
);

export default Auth;
